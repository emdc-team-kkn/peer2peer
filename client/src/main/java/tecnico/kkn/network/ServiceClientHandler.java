package tecnico.kkn.network;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.data.BidData;
import tecnico.kkn.data.ItemData;
import tecnico.kkn.service.message.RPCRequest;
import tecnico.kkn.service.message.RPCResponse;

import java.util.List;
import java.util.concurrent.CyclicBarrier;

/**
 * The class handle a connection between the client and the server
 */
public class ServiceClientHandler extends ChannelDuplexHandler {

    private static final Logger log = LoggerFactory.getLogger(ServiceClientHandler.class);
    private final ServiceClient serviceClient;
    private String lastMethod;
    private String lastRequestId;
    private final Gson gson;
    private StringBuilder messageBuilder;

    public ServiceClientHandler(ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
        this.messageBuilder = new StringBuilder();
        this.gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        ByteBuf out = (ByteBuf) msg;
        RPCRequest request = gson.fromJson(out.toString(CharsetUtil.UTF_8), RPCRequest.class);
        lastMethod = request.getMethod();
        lastRequestId = request.getId();
        super.write(ctx, msg, promise);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf in = (ByteBuf) msg;
        String message = in.toString(CharsetUtil.UTF_8).toLowerCase();
        messageBuilder.append(message);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        String message = messageBuilder.toString();
        //Reset the message builder
        messageBuilder.setLength(0);
        log.debug("Last method: {}", lastMethod);
        log.debug("Server write: {}", message);

        RPCResponse response = gson.fromJson(message, RPCResponse.class);
        if (response.getResult() != null) {
            if (lastMethod.equals("register")) {
                System.out.printf("You have successfully register as %s\n", response.getResult().get("username"));
            } else if (lastMethod.equals("login")) {
                System.out.printf("Welcome to P2PBay, %s\n", response.getResult().get("username"));
            } else if (lastMethod.equals("offer")) {
                System.out.println("Item offered successfully");
                System.out.printf("Item id: %s\n", response.getResult().get("item_id"));
            } else if (lastMethod.equals("logout")) {
                serviceClient.getItemList().clear();
                System.out.println("You have been logged out.");
            } else if (lastMethod.equals("search")) {
                int size = Integer.parseInt((String) response.getResult().get("total"));
                if (size > 0) {
                    System.out.printf("There are %d items matching your search\n", size);
                }

                List<ItemData> itemData = gson.fromJson(
                        gson.toJson(response.getResult().get("items")),
                        new TypeToken<List<ItemData>>() {
                        }.getType()
                );
                int from = Integer.parseInt((String) response.getResult().get("from"));
                int to = Integer.parseInt((String) response.getResult().get("to"));
                if (itemData != null && itemData.size() > 0) {
                    System.out.printf("Result from %d to %d \n", from + 1, to);
                    int counter = 1;
                    for (ItemData i : itemData) {
                        System.out.printf("%d. %s (current bid %.2f) \n", counter, i.getTitle(), i.getMaxBid());
                        counter++;
                    }
                    serviceClient.setItemList(itemData);
                    System.out.println("This list is now saved so you can use it later");
                    System.out.println("For example, to bid on the first item, use: bid 1 <price>");
                } else {
                    System.out.println("No more results");
                }
            } else if (lastMethod.equals("offered")) {
                int size = Integer.parseInt((String) response.getResult().get("total"));
                if (size > 0) {
                    System.out.printf("You have %d offered items\n", size);
                }

                List<ItemData> itemData = gson.fromJson(
                        gson.toJson(response.getResult().get("items")),
                        new TypeToken<List<ItemData>>() {
                        }.getType()
                );

                if (itemData != null && itemData.size() > 0) {
                    int counter = 1;
                    for (ItemData i : itemData) {
                        String status = (i.getPurchased())? "sold" : "offering";
                        System.out.printf("%d. %s (current bid %.2f) - %s\n", counter, i.getTitle(), i.getMaxBid(), status);
                        counter++;
                    }
                    serviceClient.setItemList(itemData);
                    System.out.println("This list is now saved so you can use it later");
                    System.out.println("For example, to accept the bid on the first item, use: accept 1");
                }
            } else if (lastMethod.equals("history")) {
                int bidSize = Integer.parseInt((String) response.getResult().get("total_bidded"));
                int purchaseSize = Integer.parseInt((String) response.getResult().get("total_purchased"));
                if (bidSize > 0 || purchaseSize > 0) {
                    System.out.printf("You have %d bids on items and %d purchased items\n", bidSize, purchaseSize);
                }

                List<ItemData> itemData = gson.fromJson(
                        gson.toJson(response.getResult().get("items")),
                        new TypeToken<List<ItemData>>() {
                        }.getType()
                );
                List<ItemData> purchasedItems = gson.fromJson(
                        gson.toJson(response.getResult().get("purchased_items")),
                        new TypeToken<List<ItemData>>() {
                        }.getType()
                );
                List<BidData> bidData = gson.fromJson(
                        gson.toJson(response.getResult().get("bids")),
                        new TypeToken<List<BidData>>() {
                        }.getType()
                );
                List<BidData> purchasedBids = gson.fromJson(
                        gson.toJson(response.getResult().get("purchased_bids")),
                        new TypeToken<List<BidData>>() {
                        }.getType()
                );

                if (bidData != null && bidData.size() > 0) {
                    System.out.println("\nBids:");
                    int counter = 0;
                    for (ItemData i : itemData) {
                        System.out.printf("%d. %s : your bid %.2f (max. bid %.2f) \n", counter + 1, i.getTitle(), bidData.get(counter).getBidValue(), i.getMaxBid());
                        counter++;
                    }
                    serviceClient.setItemList(itemData);
                    System.out.println("");
                    System.out.println("This list is now saved so you can use it later");
                    System.out.println("For example, to bid again on the first item, use: bid 1 <price>");
                }

                if (purchasedBids != null && purchasedBids.size() > 0) {
                    System.out.println("\nPurchased items:");
                    int counter = 0;
                    for (ItemData i : purchasedItems) {
                        System.out.printf("%d. %s (paid %.2f)\n", counter + 1, i.getTitle(), purchasedBids.get(counter).getBidValue());
                        counter++;
                    }
                }
            } else if (lastMethod.equals("details")) {
                ItemData item = gson.fromJson(
                        gson.toJson(response.getResult().get("item")),
                        new TypeToken<ItemData>() {
                        }.getType()
                );

                if (item != null) {
                    System.out.println("\nItem Details:\n");
                    System.out.printf("Title: %s\n", item.getTitle());
                    System.out.printf("Maximum Bid: %f\n", item.getMaxBid());
                    System.out.printf("Description: %s\n", item.getDescription());
                } else {
                    System.out.println("\nItem not found\n");
                }
            }
        } else if (response.getError() != null) {
            System.out.println(response.getError().get("message"));
        }
        CyclicBarrier barrier = serviceClient.getBarrier();
        // Only call await when cmdline is blocking
        if (barrier.getNumberWaiting() == 1) {
            barrier.await();
        }
    }
}