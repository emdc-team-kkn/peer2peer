package tecnico.kkn.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.data.ItemData;
import tecnico.kkn.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * This is the client class to handle connection to the P2P network
 */
public class ServiceClient {

    private final Logger log = LoggerFactory.getLogger(ServiceClient.class);

    private final CyclicBarrier barrier;

//    private String username;

    private RegisterService registerService;
    private LoginService loginService;
    private LogoutService logoutService;
    private OfferItemService offerItemService;
    private SearchService searchService;
    private BidService bidService;
    private AcceptBidService acceptBidService;
    private BidHistoryService bidHistoryService;
    private ViewOfferedService viewOfferedService;
    private DetailsService detailsService;

    private int commandCounter;
    private Channel channel;

    private List<ItemData> itemList;

    public ServiceClient(CyclicBarrier barrier) {
        this.barrier = barrier;
        this.commandCounter = 0;
        this.itemList = new ArrayList<ItemData>();
    }

    public void connect(String address, int port) throws InterruptedException {
        final EventLoopGroup loopGroup = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        final ServiceClient serviceClient = this;
        b.group(loopGroup)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new ServiceClientHandler(serviceClient));
                    }
                });

        // Connect to the server
        ChannelFuture channelFuture = b.connect(address, port);
        channelFuture.awaitUninterruptibly();

        // Get the channel
        channel = channelFuture.channel();
        // Unblock the command line when connection lost
        channel.closeFuture().addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                log.debug("Connection lost. Exiting");
                loopGroup.shutdownGracefully();
                // Only call await if the CommandLine is waiting
                if (barrier.getNumberWaiting() == 1) {
                    barrier.await();
                }
            }
        });

        registerService = new RegisterService(channel);
        loginService = new LoginService(channel);
        logoutService = new LogoutService(channel);
        offerItemService = new OfferItemService(channel);
        searchService = new SearchService(channel);
        bidService = new BidService(channel);
        viewOfferedService = new ViewOfferedService(channel);
        acceptBidService = new AcceptBidService(channel);
        bidHistoryService = new BidHistoryService(channel);
        detailsService = new DetailsService(channel);
    }

//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }

    public Channel getChannel() {
        return channel;
    }

    public CyclicBarrier getBarrier() {
        return barrier;
    }

    public void setItemList(List<ItemData> itemList) {
        this.itemList = itemList;
    }

    public List<ItemData> getItemList() {
        return itemList;
    }

    public void register(String username, String password) {
        registerService.register(username, password, String.valueOf(++commandCounter));
    }

    public void login(String username, String password) {
        loginService.login(username, password, String.valueOf(++commandCounter));
    }

    public void logout() {
        logoutService.logout(String.valueOf(++commandCounter));
    }

    public void offer(String title, String description) {
        offerItemService.offer(title, description, String.valueOf(++commandCounter));
    }

    public void viewOffered() {
        viewOfferedService.viewOffered(String.valueOf(++commandCounter));
    }

    public void search(String query, int page) {
        searchService.search(query, page, String.valueOf(++commandCounter));
    }

    public void history() {
        bidHistoryService.history(String.valueOf(++commandCounter));
    }

    public void bid (int item, double bid) {
        if (itemList.isEmpty()) {
            System.out.println("Item list is empty");
            unblockBarrier();
        } else {
            try {
                ItemData itemData = itemList.get(item - 1);
                bidService.bid(itemData.getItemId(), bid, String.valueOf(++commandCounter));
            } catch (IndexOutOfBoundsException e) {
                System.out.printf("Item id must be between 1 and %d\n", itemList.size() + 1);
            }
        }
    }

    public void accept(int itemId) {
        if (itemList.isEmpty()) {
            System.out.println("Item list is empty");
            unblockBarrier();
        } else {
            try {
                ItemData itemData = itemList.get(itemId - 1);
                acceptBidService.accept(itemData.getItemId(), String.valueOf(++commandCounter));
            } catch (IndexOutOfBoundsException e) {
                System.out.printf("Item id must be between 1 and %d\n", itemList.size() + 1);
            }
        }
    }

    public void details(int itemId) {
        if (itemList.isEmpty()) {
            System.out.println("Item list is empty");
            unblockBarrier();
        } else {
            try {
                ItemData itemData = itemList.get(itemId - 1);
                detailsService.details(itemData.getItemId(), String.valueOf(++commandCounter));
            } catch (IndexOutOfBoundsException e) {
                System.out.printf("Item id must be between 1 and %d\n", itemList.size() + 1);
            }
        }
    }

    private void unblockBarrier() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    log.warn("Unable to unblock command line");
                }
            }
        }).start();
    }
}
