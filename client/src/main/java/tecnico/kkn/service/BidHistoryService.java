package tecnico.kkn.service;

import io.netty.channel.Channel;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handles user's bid history service
 */
public class BidHistoryService extends BaseService {

    public BidHistoryService(Channel channel) {
        super(channel);
    }

    public void history(String requestId) {
        RPCRequest request = new RPCRequest();
        request.setId(requestId);
        request.setMethod("history");
        Map<String, String> params = new HashMap<String, String>();
        request.setParams(params);

        writeRequest(request);
    }
}