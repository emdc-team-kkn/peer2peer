package tecnico.kkn.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.util.CharsetUtil;
import tecnico.kkn.service.message.RPCRequest;

/**
 * Base class for our service. Provide some utilities method
 */
abstract public class BaseService {

    protected final Channel channel;
    protected final Gson gson;

    public BaseService(Channel channel) {
        this.channel = channel;
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    public void writeRequest(RPCRequest request) {
        String msg = gson.toJson(request);
        ByteBuf buf = channel.alloc().buffer();
        buf.writeBytes(msg.getBytes(CharsetUtil.UTF_8));
        buf.writeBytes("\n".getBytes(CharsetUtil.UTF_8));
        channel.writeAndFlush(buf);
    }
}
