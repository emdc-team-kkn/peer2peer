package tecnico.kkn.service;

import io.netty.channel.Channel;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handles user's offered item service
 */
public class ViewOfferedService extends BaseService {

    public ViewOfferedService(Channel channel) {
        super(channel);
    }

    public void viewOffered(String requestId) {
        RPCRequest request = new RPCRequest();
        request.setId(requestId);
        request.setMethod("offered");
        Map<String, String> params = new HashMap<String, String>();
        request.setParams(params);

        writeRequest(request);
    }
}