package tecnico.kkn.service;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.service.message.RPCRequest;

/**
 * This class handle client logout
 */
public class LogoutService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(LogoutService.class);

    public LogoutService(Channel channel) {
        super(channel);
    }

    public void logout(String requestId) {
        log.info("Logging out");

        RPCRequest request = new RPCRequest();
        request.setMethod("logout");
        request.setId(requestId);

        writeRequest(request);
    }
}
