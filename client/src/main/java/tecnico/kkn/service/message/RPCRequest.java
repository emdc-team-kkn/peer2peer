package tecnico.kkn.service.message;

import java.util.Map;

/**
 * This class represent an RPC request from client
 */
public class RPCRequest {
    private final String jsonrpc = "2.0";
    private String method;
    private Map<String, String> params;
    private String id;

    public RPCRequest() {
    }

    public RPCRequest(String method, Map<String, String> params, String id) {
        this.method = method;
        this.params = params;
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
