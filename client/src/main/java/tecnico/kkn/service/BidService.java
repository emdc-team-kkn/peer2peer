package tecnico.kkn.service;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handle bidding service
 */
public class BidService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(BidService.class);

    public BidService(Channel channel) {
        super(channel);
    }

    public void bid(String itemId, double bid_value, String requestId) {
        log.debug("Bid on item {} with price {}\n", itemId, bid_value);

        RPCRequest request = new RPCRequest();
        request.setId(requestId);
        request.setMethod("bid");

        Map<String, String> params = new HashMap<>();
        params.put("item_id", itemId);
        params.put("bid_value", String.valueOf(bid_value));
        request.setParams(params);

        writeRequest(request);
    }
}
