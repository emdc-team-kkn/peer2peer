package tecnico.kkn.service;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class provide search service for the client
 */
public class SearchService extends BaseService {
    private final static Logger log = LoggerFactory.getLogger(SearchService.class);

    public SearchService(Channel channel) {
        super(channel);
    }

    public void search(String query, int page, String requestId) {
        log.info("Search query: {}", query);

        RPCRequest request = new RPCRequest();
        request.setMethod("search");
        Map<String, String> params = new HashMap<String, String>();
        params.put("query", query);
        params.put("page", String.valueOf(page));
        request.setParams(params);
        request.setId(requestId);
        writeRequest(request);
    }
}
