package tecnico.kkn.service;

import io.netty.channel.Channel;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handle user's item offering service
 */
public class OfferItemService extends BaseService {

    public OfferItemService(Channel channel) {
        super(channel);
    }

    public void offer(String title, String description, String requestId) {
        RPCRequest request = new RPCRequest();
        request.setId(requestId);
        request.setMethod("offer");
        Map<String, String> params = new HashMap<String, String>();
        params.put("title", title);
        params.put("description", description);
        request.setParams(params);

        writeRequest(request);
    }
}
