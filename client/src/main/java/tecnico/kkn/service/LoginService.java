package tecnico.kkn.service;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class provide user's login service
 */
public class LoginService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(RegisterService.class);

    public LoginService(Channel channel) {
        super(channel);
    }

    public void login(String username, String password, String requestId) {
        log.info("Login info: {}/{}", username, password);

        RPCRequest request = new RPCRequest();
        request.setMethod("login");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        request.setParams(params);
        request.setId(requestId);

        this.writeRequest(request);
    }

}
