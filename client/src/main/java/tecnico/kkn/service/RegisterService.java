package tecnico.kkn.service;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class provide user's registration service
 */
public class RegisterService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(RegisterService.class);

    public RegisterService(Channel channel) {
        super(channel);
    }

    public void register(String username, String password, final String requestId) {
        log.info("Registration info: {}/{}", username, password);

        RPCRequest request = new RPCRequest();
        request.setMethod("register");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        request.setParams(params);
        request.setId(requestId);

        writeRequest(request);
    }

}
