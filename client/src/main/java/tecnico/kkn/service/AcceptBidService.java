package tecnico.kkn.service;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.service.message.RPCRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handles the acceptance of bids service
 */
public class AcceptBidService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(AcceptBidService.class);

    public AcceptBidService(Channel channel) {
        super(channel);
    }

    public void accept(String itemId, String requestId) {
        log.debug("Accept bid on item {}\n", itemId);

        RPCRequest request = new RPCRequest();
        request.setId(requestId);
        request.setMethod("accept");

        Map<String, String> params = new HashMap<>();
        params.put("item_id", itemId);
        request.setParams(params);

        writeRequest(request);
    }
}