package tecnico.kkn.console;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.console.command.*;
import tecnico.kkn.network.ServiceClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * This class implement the command line
 */
public class CommandLine {
    private final static Logger log = LoggerFactory.getLogger(CommandLine.class);
    private final CyclicBarrier barrier;
    private ServiceClient serviceClient;
    private String prompt = "p2pbay";
    private List<Command> commandList;

    public CommandLine(ServiceClient serviceClient, CyclicBarrier barrier) {
        this.serviceClient = serviceClient;
        this.barrier = barrier;

        this.commandList = new ArrayList<Command>();
        this.commandList.add(new LoginCommand(serviceClient));
        this.commandList.add(new RegisterCommand(serviceClient));
        this.commandList.add(new LogoutCommand(serviceClient));
        this.commandList.add(new OfferItemCommand(serviceClient, prompt));
        this.commandList.add(new SearchCommand(serviceClient));
        this.commandList.add(new BidCommand(serviceClient));
        this.commandList.add(new ViewOfferedCommand(serviceClient));
        this.commandList.add(new AcceptBidCommand(serviceClient));
        this.commandList.add(new BidHistoryCommand(serviceClient));
        this.commandList.add(new DetailsCommand(serviceClient));
        this.commandList.add(new HelpCommand(serviceClient));
    }

    public void run() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        boolean wait, matched;
        try {
            printWelcome();
            while (true) {
                wait = false;
                matched = false;

                // Print out the prompt
                printPrompt();
                // Read & parse the command
                String command = in.readLine();
                if (command == null) {
                    break;
                }
                // Trim the space from the command
                command = command.trim();

                // If the network is closed, break out of the main loop
                if (!serviceClient.getChannel().isOpen()) {
                    System.out.println("Network connection lost. Exiting");
                    break;
                }

                for(Command c: commandList) {
                    if (c.match(command)) {
                        c.run();
                        matched = true;
                        wait = true;
                        break;
                    }
                }

                if (!matched) {
                    System.out.println("Invalid command: " + command);
                }
                // If there's a valid command, wait till the response is returned from server
                if (wait && !command.equals("help")) {
                    barrier.await();
                }
                barrier.reset();
            }
        } catch (InterruptedException e) {
            log.error("Unable to connect to server. Shutting down", e);
            System.exit(-1);
        } catch (IOException e) {
            log.error("Unable to read command", e);
            System.exit(-2);
        } catch (BrokenBarrierException e) {
            log.error("No response from server", e);
            System.exit(-3);
        }
    }

    private void printWelcome() {
        System.out.println("Welcome to P2PBay client.");
        System.out.println("To proceed, please login or register");
    }

    private void printPrompt() {
        System.out.printf("%s> ", prompt);
    }

}
