package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handle the register command
 */
public class HelpCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("help");

    public HelpCommand (ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void run() {
        System.out.println("register <username> <password> - Register in the P2P network. Provide a username and password as arguments");
        System.out.println("login <username> <password>    - Log in to the P2P network with your username and password");
        System.out.println("logout                         - Log out of the P2P network");
        System.out.println("offer                          - Offer an item for sale. You will be asked to enter a title and a description");
        System.out.println("details <item_number>          - See details on an item. You will need to provide the item number");
        System.out.println("bid <item_number> <bid_amount> - Bid on an item by providing the item number and bid amount");
        System.out.println("offered                        - See all of your offered items");
        System.out.println("accept <item_number>           - Accept a bid on one of your offers. You will need to provide the item number");
        System.out.println("history                        - See all of your bids and purchased items");
        System.out.println("search <query>                 - Search for offers by providing a query string");
        System.out.println("help                           - The help command for listing this");
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        return m.matches();
    }
}