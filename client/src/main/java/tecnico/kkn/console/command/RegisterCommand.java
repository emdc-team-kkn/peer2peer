package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handle the register command
 */
public class RegisterCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("register ([a-zA-Z0-9_-]+) ([a-zA-Z0-9_-]+)");
    private String username;
    private String password;

    public RegisterCommand (ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void run() {
        serviceClient.register(username, password);
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        if (m.matches()) {
            username = m.group(1);
            password = m.group(2);
            return true;
        } else {
            return false;
        }
    }
}
