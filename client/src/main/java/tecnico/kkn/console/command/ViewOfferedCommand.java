package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles the viewing of submitted offers command
 */
public class ViewOfferedCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("offered");

    public ViewOfferedCommand(ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void run() throws IOException {
        serviceClient.viewOffered();
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        return m.matches();
    }

}