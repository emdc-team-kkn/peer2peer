package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles the viewing of bid history command
 */
public class BidHistoryCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("history");

    public BidHistoryCommand(ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void run() throws IOException {
        //Print notice
        System.out.println("Bids will be listed shortly.");
        serviceClient.history();
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        return m.matches();
    }

}