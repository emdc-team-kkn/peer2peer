package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handle a search command
 */
public class SearchCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("search (.*)|page (\\d+) search (.*)");
    private int page;
    private String query;

    public SearchCommand (ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void run() {
        serviceClient.search(query, page);
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        if (m.matches()) {
            if (m.group(1) == null) {
                page = Integer.parseInt(m.group(2));
                query = m.group(3);
            } else {
                page = 1;
                query = m.group(1);
            }
            return true;
        } else {
            return false;
        }
    }
}
