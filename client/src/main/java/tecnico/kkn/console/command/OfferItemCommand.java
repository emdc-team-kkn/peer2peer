package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handle the offer command
 */
public class OfferItemCommand implements Command {

    private final ServiceClient serviceClient;
    private final String prompt;
    private Pattern pattern = Pattern.compile("offer");

    public OfferItemCommand(ServiceClient serviceClient, String prompt) {
        this.serviceClient = serviceClient;
        this.prompt = prompt;
    }

    @Override
    public void run() throws IOException {
        //Read title
        printSubprompt("title");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String title = in.readLine();

        //Read description
        printSubprompt("description");
        System.out.println("(Leave a blank line to end description)");
        StringBuilder description = new StringBuilder();

        //Read until encounter blank line
        String line;
        while(!(line = in.readLine()).equals("")) {
            description.append(line);
            description.append("\n");
        }
        serviceClient.offer(title, description.toString());
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        return m.matches();
    }

    private void printSubprompt(String subprompt) {
        System.out.printf("%s> %s> ", prompt, subprompt);
    }
}
