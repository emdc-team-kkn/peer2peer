package tecnico.kkn.console.command;

import java.io.IOException;

/**
 * The interface for our command class
 */
public interface Command {

    /**
     * Match a command
     * @param cmd
     * @return
     */
    public boolean match(String cmd);

    /**
     * Run a command after matching
     */
    public void run() throws IOException;
}
