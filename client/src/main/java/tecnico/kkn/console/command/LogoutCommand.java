package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handle the logout command
 */
public class LogoutCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("logout");

    public LogoutCommand (ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public void run() {
        serviceClient.logout();
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        return m.matches();
    }

}
