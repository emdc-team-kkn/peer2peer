package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handle Bid command
 */
public class BidCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("bid (\\d+) ([\\d.]+)");

    private int itemId;
    private double bidPrice;

    public BidCommand(ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        if (m.matches()) {
            itemId = Integer.parseInt(m.group(1));
            bidPrice = Double.parseDouble(m.group(2));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void run() throws IOException {
        serviceClient.bid(itemId, bidPrice);
    }
}
