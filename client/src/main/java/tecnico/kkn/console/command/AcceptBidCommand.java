package tecnico.kkn.console.command;

import tecnico.kkn.network.ServiceClient;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles the Accepting Bid command
 */
public class AcceptBidCommand implements Command {

    private final ServiceClient serviceClient;
    private Pattern pattern = Pattern.compile("accept (\\d+)");

    private int itemId;

    public AcceptBidCommand(ServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @Override
    public boolean match(String cmd) {
        Matcher m = pattern.matcher(cmd);
        if (m.matches()) {
            itemId = Integer.parseInt(m.group(1));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void run() throws IOException {
        serviceClient.accept(itemId);
    }
}