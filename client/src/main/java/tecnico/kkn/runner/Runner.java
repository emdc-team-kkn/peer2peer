package tecnico.kkn.runner;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.console.CommandLine;
import tecnico.kkn.network.ServiceClient;

import java.util.concurrent.CyclicBarrier;

/**
 * The runner class for the application
 */
public class Runner {
    private final static Logger log = LoggerFactory.getLogger(Runner.class);

    public static void main(String[] args) throws InterruptedException {
        //We use a cyclic barrier to synchornize the console and network thread
        CyclicBarrier barrier = new CyclicBarrier(2);

        ServiceClient serviceClient = new ServiceClient(barrier);
        serviceClient.connect(args[0], Integer.parseInt(args[1]));

        if (serviceClient.getChannel().isOpen()) {
            CommandLine commandLine = new CommandLine(serviceClient, barrier);
            commandLine.run();
        } else {
            log.error("Unable to connect to the server");
            System.out.println("Unable to connect to the server");
        }

    }
}
