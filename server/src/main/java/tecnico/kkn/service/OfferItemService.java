package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.BaseFuture;
import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.executor.OfferExecutor;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

/**
 * This class handle an user's item offering service
 */
public class OfferItemService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(OfferItemService.class);

    private final ExecutorService executorService;

    public OfferItemService(Node node, ChannelHandlerContext ctx, ExecutorService executorService) {
        super(node, ctx);
        this.executorService = executorService;
    }

    public void offerItem(final String title, String description, final String requestId) throws IOException {
        log.debug("Offer item query: {}", title);
        // Since the offer item is complicated, we run it synchronously using a QueryExecutor
        // and submit the executor to run outside of Netty IO loop
        executorService.submit(new OfferExecutor(node, ctx, title, description, requestId));
    }
}
