package tecnico.kkn.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.CharsetUtil;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.RPCResponse;

/**
 * Base class for our service. Provide some utilities method
 */
abstract public class BaseService {

    protected final Node node;
    protected ChannelHandlerContext ctx;
    protected Gson gson;

    public BaseService(Node node, ChannelHandlerContext ctx) {
        this.node = node;
        this.ctx = ctx;
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    public void writeResponse(RPCResponse response) {
        String message = gson.toJson(response);
        ByteBuf out = ctx.alloc().buffer(message.length());
        out.writeBytes(message.getBytes(CharsetUtil.UTF_8));
        ctx.writeAndFlush(out);
    }

}

