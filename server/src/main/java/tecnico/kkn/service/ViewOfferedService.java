package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.executor.OfferExecutor;
import tecnico.kkn.service.executor.ViewOfferedExecutor;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * This class handles a user's offered item service
 */
public class ViewOfferedService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(ViewOfferedService.class);

    private final ExecutorService executorService;

    public ViewOfferedService(Node node, ChannelHandlerContext ctx, ExecutorService executorService) {
        super(node, ctx);
        this.executorService = executorService;
    }

    public void viewOffered(final String requestId) throws IOException {
        //log.debug("Offer item query: {}", title);
        // Since the offer item is complicated, we run it synchronously using a QueryExecutor
        // and submit the executor to run outside of Netty IO loop
        executorService.submit(new ViewOfferedExecutor(node, ctx, requestId));
    }
}