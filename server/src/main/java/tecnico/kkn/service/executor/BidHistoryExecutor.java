package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.BidData;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class handles the searching of a user's offered items in our DHT
 */
public class BidHistoryExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(BidHistoryExecutor.class);

    public BidHistoryExecutor(Node node,
                               ChannelHandlerContext ctx,
                               String requestId) {
        super(node, ctx, requestId);
    }

    @Override
    public void run() {
        ServiceServerHandler handler = (ServiceServerHandler) ctx.handler();
        Map<Number160, Data> result;
        final Number160 userKey = Number160.createHash(handler.getUsername());

        List<ItemData> bidItems = new ArrayList<ItemData>();
        List<ItemData> purchasedItems = new ArrayList<ItemData>();
        List<BidData> bidData = new ArrayList<BidData>();
        List<BidData> purchasedBids = new ArrayList<BidData>();

        // Store the item in the DHT
        final Peer peer = node.getNextPeer();
        RPCResponse response = new RPCResponse();
        response.setId(requestId);

        try {
            FutureDHT userBidFuture = peer.get(userKey)
                    .setDomainKey(Node.USER_BID_DOMAIN)
                    .setAll()
                    .start();

            userBidFuture.awaitUninterruptibly();

            if (userBidFuture.isSuccess()) {
                List<ItemData> itemData = new ArrayList<ItemData>();
                result = userBidFuture.getDataMap();
                List<Data> allData = new ArrayList<Data>(result.values());

                for(Data d: allData) {
                    String bidId = (String) d.getObject();
                    Number160 bidKey = Number160.createHash(bidId);
                    FutureDHT bidFuture = peer
                            .get(bidKey)
                            .setDomainKey(Node.BID_DOMAIN)
                            .start();

                    bidFuture.awaitUninterruptibly();

                    if (bidFuture.isSuccess()) {
                        BidData bid = (BidData) bidFuture.getData().getObject();
                        try {
                            FutureDHT itemFuture = peer.get(Number160.createHash(bid.getItemId())).setDomainKey(Node.ITEM_DOMAIN).start().awaitUninterruptibly();
                            if (itemFuture.isSuccess()) {
                                ItemData item = (ItemData) itemFuture.getData().getObject();
                                log.debug("Item title: {}, item id: {}", item.getTitle(), item.getItemId());
                                if (bid.getPurchased()) {
                                    purchasedBids.add(bid);
                                    purchasedItems.add(item);
                                } else {
                                    bidData.add(bid);
                                    bidItems.add(item);
                                }
                            }
                        } catch (ClassCastException e) {
                            log.debug("Invalid class", e);
                        }
                    }
                }

                Map<String, Object> ret = new HashMap<String, Object>();
                ret.put("purchased_items", purchasedItems);
                ret.put("purchased_bids", purchasedBids);
                ret.put("items", bidItems);
                ret.put("bids", bidData);
                ret.put("total_purchased", String.valueOf(purchasedBids.size()));
                ret.put("total_bidded", String.valueOf(bidData.size()));
                response.setResult(ret);

            } else {
                response.setError(tecnico.kkn.service.message.Error.ERR_MISC_CODE, tecnico.kkn.service.message.Error.ERR_MISC_MSG);
            }
            writeResponse(response);
        } catch (Exception e) {
            log.error("Unknown exception", e);
            response.setError(tecnico.kkn.service.message.Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
        }
    }
}