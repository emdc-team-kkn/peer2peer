package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class handle saving an offered item to our DHT
 */
public class OfferExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(OfferExecutor.class);
    private String title;
    private String description;

    public OfferExecutor(Node node,
                         ChannelHandlerContext ctx,
                         String title,
                         String description,
                         String requestId) {
        super(node, ctx, requestId);
        this.title = title;
        this.description = description;
    }

    @Override
    public void run() {
        log.debug("Item title: {}", title);
        ServiceServerHandler handler = (ServiceServerHandler) ctx.handler();
        final String itemId = UUID.randomUUID().toString();

        RPCResponse response = new RPCResponse();
        response.setId(requestId);

        if (title.isEmpty()) {
            // Do not allow item with empty title
            response.setError(Error.ERR_ITEM_EMPTY_TITLE_CODE, Error.ERR_ITEM_EMPTY_TITLE_MSG);
            writeResponse(response);
            return;
        }
        final ItemData item = new ItemData();
        item.setItemId(itemId);
        item.setTitle(title);
        item.setDescription(description);
        item.setOwnerName(handler.getUsername());
        final Number160 itemKey = Number160.createHash(itemId);
        final Number160 userKey = Number160.createHash(handler.getUsername());

        // Store the item in the DHT
        final Peer peer = node.getNextPeer();

        try {
            FutureDHT itemFuture = peer
                    .put(itemKey)
                    .setDomainKey(Node.ITEM_DOMAIN)
                    .setData(new Data(item))
                    .start();
            FutureDHT userItemFuture = peer
                    .put(userKey)
                    .setDomainKey(Node.USER_ITEM_DOMAIN)
                    .setData(itemKey, new Data(itemId))
                    .start();

            itemFuture.awaitUninterruptibly();
            userItemFuture.awaitUninterruptibly();

            if (userItemFuture.isSuccess() && itemFuture.isSuccess()) {
                synchronized (node) {
                    node.getNodeStatistics().getItemData().addSum(1);
                }

                Map<String, Object> result = new HashMap<String, Object>();
                result.put("item_id", itemId);
                response.setResult(result);

                // Index the item with the title
                String[] tokens = title.split("\\s+");
                for (final String token : tokens) {
                    Number160 tokenKey = Number160.createHash(token);
                    log.debug("Token hash: {}", tokenKey);
                    // TODO: Check if the item is index successfully
                    FutureDHT indexFuture = peer.put(tokenKey).setDomainKey(Node.ITEM_INDEX_DOMAIN)
                            .setData(Number160.createHash(itemId), new Data(itemId))
                            .start();
                    indexFuture.addListener(new BaseFutureAdapter<FutureDHT>() {
                        @Override
                        public void operationComplete(FutureDHT future) throws Exception {
                            if (future.isSuccess()) {
                                log.debug("Index successful: {} - {}", token, itemId);
                            } else {
                                log.debug("Index failed {} - {}", token, itemId);
                            }
                        }
                    });
                }
            } else {
                response.setError(tecnico.kkn.service.message.Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
            }
            writeResponse(response);
        } catch (Exception e) {
            log.error("Unknown exception", e);
            response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
        }
    }
}
