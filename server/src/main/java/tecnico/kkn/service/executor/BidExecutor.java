package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.p2p.BidData;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class handle the bid query against our DHTs
 */
public class BidExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(BidExecutor.class);
    private final String itemId;
    private final String username;
    private final double bidValue;

    public BidExecutor(Node n, ChannelHandlerContext ctx, String itemId, String username, double bidValue, String requestId) {
        super(n, ctx, requestId);
        this.itemId = itemId;
        this.username = username;
        this.bidValue = bidValue;
    }

    @Override
    public void run() {
        final Peer peer = node.getNextPeer();

        final String bidId = UUID.randomUUID().toString();
        final BidData bid = new BidData();
        bid.setBidId(bidId);
        bid.setBidValue(bidValue);
        bid.setItemId(itemId);
        bid.setUserId(username);

        final Number160 bidIdHash = Number160.createHash(bidId);
        final Number160 itemIdHash = Number160.createHash(itemId);
        final Number160 userIdHash = Number160.createHash(username);

        RPCResponse response = new RPCResponse();
        response.setId(requestId);
        try {
            // Store the bid in the DHT
            final FutureDHT bidFuture = peer.put(bidIdHash)
                    .setDomainKey(Node.BID_DOMAIN)
                    .setData(new Data(bid))
                    .start();
            // Store mapping from item to bid
            final FutureDHT bidItemFuture = peer
                    .put(itemIdHash)
                    .setDomainKey(Node.ITEM_BID_DOMAIN)
                    .setData(bidIdHash, new Data(bidId))
                    .start();
            // Store mapping from user to bid
            final FutureDHT bidUserFuture = peer
                    .put(userIdHash)
                    .setDomainKey(Node.USER_BID_DOMAIN)
                    .setData(bidIdHash, new Data(bidId))
                    .start();
            // Get the item data
            final FutureDHT itemFuture = peer
                    .get(itemIdHash)
                    .setDomainKey(Node.ITEM_DOMAIN)
                    .start();

            // Block till all 4 query are complete
            bidItemFuture.awaitUninterruptibly();
            bidUserFuture.awaitUninterruptibly();
            itemFuture.awaitUninterruptibly();
            bidFuture.awaitUninterruptibly();

            if (itemFuture.isSuccess() && bidFuture.isSuccess() && bidItemFuture.isSuccess() && bidUserFuture.isSuccess()) {
                ItemData itemData = (ItemData) itemFuture.getData().getObject();
                boolean error = false;
                if (itemData.getOwnerName().equals(username)) {
                    response.setId(requestId);
                    response.setError(
                            Error.ERR_BID_OWN_ITEM_CODE,
                            Error.ERR_BID_OWN_ITEM_MSG
                    );
                    writeResponse(response);
                    error = true;
                } else if (itemData.getMaxBid() > bidValue) {
                    // Return error to the user
                    response.setId(requestId);
                    response.setError(
                            Error.ERR_LOW_BID_CODE,
                            String.format(Error.ERR_LOW_BID_MSG, itemData.getMaxBid())
                    );
                    writeResponse(response);
                    error = true;
                } else {
                    // Write the new max bid value into the item
                    itemData.setMaxBid(bidValue);
                    FutureDHT updatedItem = peer.put(itemIdHash)
                            .setDomainKey(Node.ITEM_DOMAIN)
                            .setData(new Data(itemData))
                            .start();
                    updatedItem.awaitUninterruptibly();

                    if(updatedItem.isSuccess()) {
                        Map<String, Object> result = new HashMap<String, Object>();
                        result.put("bid_id", bidId);
                        response.setResult(result);
                        writeResponse(response);
                    } else {
                        response.setError(Error.ERR_NOT_BID_CODE, Error.ERR_NOT_BID_MSG);
                        writeResponse(response);
                    }
                }
                if (error) {
                    // Invalid bid, delete the bid
                    peer.remove(itemIdHash)
                            .setDomainKey(Node.ITEM_BID_DOMAIN)
                            .setContentKey(bidIdHash)
                            .setRepetitions(6)
                            .start()
                            .awaitUninterruptibly();
                    peer.remove(userIdHash)
                            .setDomainKey(Node.USER_BID_DOMAIN)
                            .setContentKey(bidIdHash)
                            .setRepetitions(6)
                            .start()
                            .awaitUninterruptibly();
                    peer.remove(bidIdHash)
                            .setDomainKey(Node.BID_DOMAIN)
                            .setRepetitions(6)
                            .start()
                            .awaitUninterruptibly();
                }
            } else {
                response.setError(Error.ERR_NOT_BID_CODE, Error.ERR_NOT_BID_MSG);
                writeResponse(response);
            }
        } catch (IOException | ClassNotFoundException e) {
            log.debug("Unhandled exception", e);
            response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
            writeResponse(response);
        }

    }
}