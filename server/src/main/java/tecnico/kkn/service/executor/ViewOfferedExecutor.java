package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.util.*;

/**
 * This class handles the searching of a user's offered items in our DHT
 */
public class ViewOfferedExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(ViewOfferedExecutor.class);

    public ViewOfferedExecutor(Node node,
                         ChannelHandlerContext ctx,
                         String requestId) {
        super(node, ctx, requestId);
    }

    @Override
    public void run() {
        ServiceServerHandler handler = (ServiceServerHandler) ctx.handler();
        Map<Number160, Data> result;
        final Number160 userKey = Number160.createHash(handler.getUsername());

        // Store the item in the DHT
        final Peer peer = node.getNextPeer();
        RPCResponse response = new RPCResponse();
        response.setId(requestId);

        Map<String, Object> ret = new HashMap<String, Object>();

        try {
            FutureDHT userItemFuture = peer.get(userKey)
                    .setDomainKey(Node.USER_ITEM_DOMAIN)
                    .setAll()
                    .start();

            userItemFuture.awaitUninterruptibly();

            if (userItemFuture.isSuccess()) {
                List<ItemData> itemData = new ArrayList<ItemData>();
                result = userItemFuture.getDataMap();
                List<Data> allData = new ArrayList<Data>(result.values());

                for(Data d: allData) {
                    String itemId = (String) d.getObject();
                    Number160 itemKey = Number160.createHash(itemId);
                    FutureDHT itemFuture = peer
                            .get(itemKey)
                            .setDomainKey(Node.ITEM_DOMAIN)
                            .start();

                    itemFuture.awaitUninterruptibly();

                    if (itemFuture.isSuccess()) {
                        try {
                            ItemData data = (ItemData) itemFuture.getData().getObject();
                            itemData.add(data);
                            log.debug("Item title: {}, item id: {}", data.getTitle(), data.getItemId());
                        } catch (ClassCastException e) {
                            log.debug("Invalid class", e);
                            // Delete invalid item
                            peer.remove(Number160.createHash(itemId)).setDomainKey(Node.ITEM_DOMAIN).start().awaitUninterruptibly();
                        }
                    }
                }

                ret.put("items", itemData);
                ret.put("total", String.valueOf(itemData.size()));
                response.setResult(ret);

            } else {
                response.setError(tecnico.kkn.service.message.Error.ERR_MISC_CODE, tecnico.kkn.service.message.Error.ERR_MISC_MSG);
            }
            writeResponse(response);
        } catch (Exception e) {
            log.error("Unknown exception", e);
            response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
        }
    }
}