package tecnico.kkn.service.executor;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.CharsetUtil;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.RPCResponse;

/**
 * Base class for our service executor
 * We need to use an service executor if we want to perform long running synchronous
 * operation to avoid blocking Netty IO thread
 */
public abstract class BaseExecutor implements Runnable {

    protected final Node node;
    protected final ChannelHandlerContext ctx;
    protected final Gson gson;
    protected final String requestId;

    public BaseExecutor(Node node, ChannelHandlerContext ctx, String requestId) {
        this.node = node;
        this.ctx = ctx;
        this.requestId = requestId;
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    public void writeResponse(RPCResponse response) {
        String message = gson.toJson(response);
        ByteBuf out = ctx.alloc().buffer(message.length());
        out.writeBytes(message.getBytes(CharsetUtil.UTF_8));
        ctx.writeAndFlush(out);
    }
}
