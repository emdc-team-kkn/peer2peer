package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exceptions.InvalidQueryException;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;
import tecnico.kkn.service.search.Query;
import tecnico.kkn.service.search.QueryParser;

import java.util.*;

/**
 * This is class handle a search query against our DHT
 */
public class SearchExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(SearchExecutor.class);
    private final QueryParser parser;
    private final String queryString;
    private final int page;

    public SearchExecutor(Node n, ChannelHandlerContext ctx, String queryString, int page, String requestId) {
        super(n, ctx, requestId);
        this.parser = new QueryParser();
        this.queryString = queryString;
        this.page = page;
    }

    @Override
    public void run() {
        RPCResponse response = new RPCResponse();
        try {
            Query query = parser.parse(queryString);
            Map<Number160, Data> result = query.execute(node);
            List<Data> allData = new ArrayList<Data>(result.values());
            int start = page * 10 - 10;
            int end = page * 10;
            List<Data> pageData;
            if (start > allData.size()) {
                start = 0;
                end = 0;
                pageData = new ArrayList<Data>();
            } else if (end > allData.size()) {
                end = allData.size();
                pageData = allData.subList(start, end);
            } else {
                pageData = allData.subList(start, end);
            }
            List<ItemData> itemDatas = new ArrayList<ItemData>();

            for(Data d: pageData) {
                String itemId = (String) d.getObject();
                Peer peer = node.getNextPeer();
                FutureDHT future = peer.get(Number160.createHash(itemId)).setDomainKey(Node.ITEM_DOMAIN).start();
                future.awaitUninterruptibly();

                if (future.isSuccess()) {
                    try {
                        ItemData data = (ItemData) future.getData().getObject();
                        itemDatas.add(data);
                        log.debug("Item title: {}, item id: {}", data.getTitle(), data.getItemId());
                    } catch (ClassCastException e) {
                        log.debug("Invalid class", e);
                        // Delete invalid item
                        peer.remove(Number160.createHash(itemId)).setDomainKey(Node.ITEM_DOMAIN).start().awaitUninterruptibly();
                    }
                }
            }
            Map<String, Object> ret = new HashMap<String, Object>();
            ret.put("items", itemDatas);
            ret.put("total", String.valueOf(allData.size()));
            ret.put("from", String.valueOf(start));
            ret.put("to", String.valueOf(end));
            response.setResult(ret);

        } catch (InvalidQueryException e) {
            log.error("Invalid query", e);
            response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
        } catch (Exception e) {
            log.error("Unknown exception", e);
            response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
        }
        writeResponse(response);
    }
}
