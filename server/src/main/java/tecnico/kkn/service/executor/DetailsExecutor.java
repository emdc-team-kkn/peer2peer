package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class handles the details of items in our DHTs
 */
public class DetailsExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(DetailsExecutor.class);
    private final String itemId;

    public DetailsExecutor(Node n, ChannelHandlerContext ctx, String itemId, String requestId) {
        super(n, ctx, requestId);
        this.itemId = itemId;
    }

    @Override
    public void run() {
        RPCResponse response = new RPCResponse();
        response.setId(requestId);

        log.debug("Details on item {}", itemId);

        final Peer peer = node.getNextPeer();
        final Number160 itemKey = Number160.createHash(itemId);

        final FutureDHT itemFuture = peer
                .get(itemKey)
                .setDomainKey(Node.ITEM_DOMAIN)
                .start();

        itemFuture.awaitUninterruptibly();

        if (itemFuture.isSuccess()) {
            try {
                ItemData itemData = (ItemData) itemFuture.getData().getObject();

                Map<String, Object> res = new HashMap<String, Object>();
                res.put("item", itemData);
                response.setResult(res);
                writeResponse(response);

            } catch (IOException | ClassNotFoundException e) {
                log.debug("Unhandled exception", e);
                response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
                writeResponse(response);
            }
        }
    }
}