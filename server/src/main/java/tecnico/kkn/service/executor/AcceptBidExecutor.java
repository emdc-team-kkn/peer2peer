package tecnico.kkn.service.executor;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.BidData;
import tecnico.kkn.p2p.ItemData;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.*;
import tecnico.kkn.service.message.Error;

import java.io.IOException;
import java.util.*;

/**
 * This class handles the acceptance of bids in our DHTs
 */
public class AcceptBidExecutor extends BaseExecutor {

    private final static Logger log = LoggerFactory.getLogger(AcceptBidExecutor.class);
    private final String itemId;

    public AcceptBidExecutor(Node n, ChannelHandlerContext ctx, String itemId, String requestId) {
        super(n, ctx, requestId);
        this.itemId = itemId;
    }

    @Override
    public void run() {
        RPCResponse response = new RPCResponse();
        response.setId(requestId);

        ServiceServerHandler handler = (ServiceServerHandler) ctx.handler();
        log.debug("Accept bid on item {}, user {}", itemId, handler.getUsername());
        // We run the bid query on a different thread to avoid blocking Netty IO thread
//        executorService.submit(new BidExecutor(node, ctx, itemId, requestId));

        final Peer peer = node.getNextPeer();
        final Number160 itemKey = Number160.createHash(itemId);

        final FutureDHT itemFuture = peer
                .get(itemKey)
                .setDomainKey(Node.ITEM_DOMAIN)
                .start();

        itemFuture.awaitUninterruptibly();

        if (itemFuture.isSuccess()) {
            try {
                ItemData itemData = (ItemData) itemFuture.getData().getObject();

                FutureDHT bidData = peer.get(itemKey).setDomainKey(Node.ITEM_BID_DOMAIN).setAll().start();
                bidData.awaitUninterruptibly();

                if (bidData.isSuccess() && !bidData.getDataMap().isEmpty()) {
                    Map<Number160, Data> result = bidData.getDataMap();
                    List<Data> bidList = new ArrayList<Data>(result.values());
                    for (Data b: bidList) {
                        String bidId = (String) b.getObject();
                        FutureDHT bidFuture = peer.get(Number160.createHash(bidId)).setDomainKey(Node.BID_DOMAIN).start().awaitUninterruptibly();
                        if (bidFuture.isSuccess()) {
                            BidData bid = (BidData) bidFuture.getData().getObject();
                            if (bid.getBidValue() == itemData.getMaxBid()) {
                                bid.setPurchased(true);
                                FutureDHT updatedBid = peer.put(Number160.createHash(bid.getBidId()))
                                        .setDomainKey(Node.BID_DOMAIN)
                                        .setData(new Data(bid))
                                        .start();
                                updatedBid.awaitUninterruptibly();
                                break;
                            }
                        }
                    }

                    itemData.setPurchased(true);

                    FutureDHT updatedItem = peer.put(itemKey)
                            .setDomainKey(Node.ITEM_DOMAIN)
                            .setData(new Data(itemData))
                            .start();

                    updatedItem.awaitUninterruptibly();

                    if(updatedItem.isSuccess()) {
                        Map<String, Object> res = new HashMap<String, Object>();
                        res.put("item_id", itemId);
                        response.setResult(res);
                        writeResponse(response);
                    } else {
                        response.setError(Error.ERR_NOT_BID_CODE, Error.ERR_NOT_BID_MSG);
                        writeResponse(response);
                    }
                } else {
                    response.setError(Error.ERR_NOT_BID_CODE, Error.ERR_NOT_BID_MSG);
                    writeResponse(response);
                }

            } catch (IOException | ClassNotFoundException e) {
                log.debug("Unhandled exception", e);
                response.setError(tecnico.kkn.service.message.Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
                writeResponse(response);
            }
        }
    }
}