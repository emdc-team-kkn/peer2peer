package tecnico.kkn.service.message;

/**
 * This class contain error message and code
 */
public class Error {
    public static final String ERR_LOGGED_IN_MSG = "You are already logged in";
    public static final String ERR_LOGGED_IN_CODE = "410";

    public static final String ERR_USER_EXISTS_MSG = "This user already exists";
    public static final String ERR_USER_EXISTS_CODE = "409";

    public static final String ERR_MISC_MSG = "Server error";
    public static final String ERR_MISC_CODE = "500";

    public static final String ERR_INVALID_USER_MSG = "Invalid user/password";
    public static final String ERR_INVALID_USER_CODE = "403";

    public static final String ERR_NOT_AUTH_MSG = "Login required";
    public static final String ERR_NOT_AUTH_CODE = "401";

    public static final String ERR_NOT_BID_MSG = "The server cannot place your bid. Try again later";
    public static final String ERR_NOT_BID_CODE = "402";

    public static final String ERR_LOW_BID_MSG = "Your bid is too low. The current bid is %.2f";
    public static final String ERR_LOW_BID_CODE = "405";

    public static final String ERR_BID_OWN_ITEM_MSG = "You cannot place bid on your own item";
    public static final String ERR_BID_OWN_ITEM_CODE = "406";

    public static final String ERR_ITEM_EMPTY_TITLE_MSG = "Item title cannot be empty";
    public static final String ERR_ITEM_EMPTY_TITLE_CODE = "407";
}
