package tecnico.kkn.service.message;

import java.util.HashMap;
import java.util.Map;

/**
 * This class represent an RPC Response to return to clients
 */
public class RPCResponse {
    private final String jsonrpc = "2.0";
    private Map<String, Object> result;
    private Map<String, String> error;
    private String id;

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    public Map<String, String> getError() {
        return error;
    }

    public void setError(Map<String, String> error) {
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setError(String code, String message) {
        this.error = new HashMap<String, String>();
        this.error.put("code", code);
        this.error.put("message", message);
    }
}
