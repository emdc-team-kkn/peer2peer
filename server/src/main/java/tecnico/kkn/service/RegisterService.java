package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.p2p.UserData;
import tecnico.kkn.service.message.Error;
import tecnico.kkn.service.message.RPCResponse;
import tecnico.kkn.utils.PasswordHash;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class provide user's registration service
 */
public class RegisterService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(RegisterService.class);

    public RegisterService(Node node, ChannelHandlerContext ctx) {
        super(node, ctx);
    }

    public void register(final String username,
                              final String password,
                              final String requestId)
            throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        log.info("Registration info: {}/{}", username, password);

        final Number160 userKey = Number160.createHash(username);
        log.debug("User key: {}", userKey.toString());
        final Peer p = node.getNextPeer();

        final UserData user = new UserData();
        user.setUsername(username);
        user.setPasswordHash(PasswordHash.createHash(password));

        //Check if the user already existed
        FutureDHT future = p.get(userKey).setDomainKey(Node.USER_DOMAIN).start();
        future.addListener(new BaseFutureAdapter<FutureDHT>() {
            @Override
            public void operationComplete(FutureDHT future) throws Exception {
                if (future.isSuccess()) {
                    log.debug("User {} already exists", username);
                    RPCResponse response = new RPCResponse();
                    response.setError(Error.ERR_USER_EXISTS_CODE, Error.ERR_USER_EXISTS_MSG);
                    writeResponse(response);
                } else {
                    FutureDHT future2 = p.put(userKey)
                            .setDomainKey(Node.USER_DOMAIN)
                            .setData(new Data(user))
                            .start();
                    future2.addListener(new BaseFutureAdapter<FutureDHT>() {
                        @Override
                        public void operationComplete(FutureDHT future) throws Exception {
                            RPCResponse response = new RPCResponse();
                            response.setId(requestId);

                            if (future.isSuccess()) {
                                synchronized (node) {
                                    node.getNodeStatistics().getUserData().addSum(1);
                                }
                                ServiceServerHandler handler = ((ServiceServerHandler) ctx.handler());
                                handler.setAuthenticated(true);
                                handler.setUsername(username);
                                Map<String, Object> result = new HashMap<String, Object>();
                                result.put("username", user.getUsername());
                                response.setResult(result);
                                response.setId(requestId);
                                writeResponse(response);
                            } else {
                                response.setError(Error.ERR_MISC_CODE, Error.ERR_MISC_MSG);
                                writeResponse(response);
                            }
                        }
                    });
                }
            }
        });
    }
}
