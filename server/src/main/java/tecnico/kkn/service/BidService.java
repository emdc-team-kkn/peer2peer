package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.executor.BidExecutor;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * This class handle item bidding service
 */
public class BidService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(BidService.class);
    private ExecutorService executorService;

    public BidService(Node node, ChannelHandlerContext ctx, ExecutorService executorService) {
        super(node, ctx);
        this.executorService = executorService;
    }

    public void bid(String itemId, double bidValue, String username, String requestId) {
        log.debug("Bid on item {}, value {}, user {}", itemId, bidValue, username);
        // We run the bid query on a different thread to avoid blocking Netty IO thread
        executorService.submit(new BidExecutor(node, ctx, itemId, username, bidValue, requestId));
    }

}

