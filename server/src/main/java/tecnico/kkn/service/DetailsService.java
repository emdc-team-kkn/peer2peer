package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.executor.AcceptBidExecutor;
import tecnico.kkn.service.executor.DetailsExecutor;

import java.util.concurrent.ExecutorService;

/**
 * This class handles the details on an item service
 */
public class DetailsService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(DetailsService.class);
    private ExecutorService executorService;

    public DetailsService(Node node, ChannelHandlerContext ctx, ExecutorService executorService) {
        super(node, ctx);
        this.executorService = executorService;
    }

    public void details(String itemId, String requestId) {
        log.debug("Details on item {}", itemId);
        // We run the bid query on a different thread to avoid blocking Netty IO thread
        executorService.submit(new DetailsExecutor(node, ctx, itemId, requestId));
    }

}