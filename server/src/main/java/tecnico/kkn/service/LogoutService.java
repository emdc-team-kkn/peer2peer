package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.message.RPCResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handle client logout
 */
public class LogoutService extends BaseService {

    public LogoutService(Node node, ChannelHandlerContext ctx) {
        super(node, ctx);
    }

    public void logout(String requestId) {
        ServiceServerHandler handler = (ServiceServerHandler) ctx.handler();
        handler.setAuthenticated(false);
        handler.setUsername(null);

        RPCResponse response = new RPCResponse();
        Map<String, Object> result = new HashMap<String, Object>();
        response.setId(requestId);
        response.setResult(result);

        writeResponse(response);
    }
}
