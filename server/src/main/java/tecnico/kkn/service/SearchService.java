package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exceptions.InvalidQueryException;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.executor.SearchExecutor;

import java.util.concurrent.ExecutorService;

/**
 * This class provide search service
 */
public class SearchService extends BaseService {
    private final static Logger log = LoggerFactory.getLogger(SearchService.class);
    private final ExecutorService executorService;

    public SearchService(Node node, ChannelHandlerContext ctx, ExecutorService executorService) {
        super(node, ctx);
        this.executorService = executorService;
    }

    public void search(String query, String page, String requestId) throws InvalidQueryException {
        log.debug("Search query: {}, page: {}", query, page);
        // Since the search query is complicated, we run it synchronously using a QueryExecutor
        // and submit the executor to run outside of Netty IO loop
        executorService.submit(new SearchExecutor(node, ctx, query, Integer.parseInt(page), requestId));
    }
}
