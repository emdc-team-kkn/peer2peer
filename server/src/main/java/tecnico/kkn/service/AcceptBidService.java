package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.executor.AcceptBidExecutor;

import java.util.concurrent.ExecutorService;

/**
 * This class handles the acceptance of bids service
 */
public class AcceptBidService extends BaseService {

    private final static Logger log = LoggerFactory.getLogger(AcceptBidService.class);
    private ExecutorService executorService;

    public AcceptBidService(Node node, ChannelHandlerContext ctx, ExecutorService executorService) {
        super(node, ctx);
        this.executorService = executorService;
    }

    public void accept(String itemId, String requestId) {
        log.debug("Accept bid on item {}", itemId);
        // We run the bid query on a different thread to avoid blocking Netty IO thread
        executorService.submit(new AcceptBidExecutor(node, ctx, itemId, requestId));
    }

}