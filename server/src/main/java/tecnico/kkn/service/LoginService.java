package tecnico.kkn.service;

import io.netty.channel.ChannelHandlerContext;
import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.p2p.UserData;
import tecnico.kkn.service.message.Error;
import tecnico.kkn.service.message.RPCResponse;
import tecnico.kkn.network.ServiceServerHandler;
import tecnico.kkn.utils.PasswordHash;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handle login request from a client
 */
public class LoginService extends BaseService {
    private final static Logger log = LoggerFactory.getLogger(RegisterService.class);

    public LoginService(Node node, ChannelHandlerContext ctx) {
        super(node, ctx);
    }

    public void login(final String username, final String password, final String requestId) {
        log.info("Registration info: {}/{}", username, password);

        final Number160 userKey = Number160.createHash(username);
        log.debug("User key: {}", userKey.toString());
        final Peer p = node.getNextPeer();

        FutureDHT future = p.get(userKey).setDomainKey(Node.USER_DOMAIN).start();
        future.addListener(new BaseFutureAdapter<FutureDHT>() {
            @Override
            public void operationComplete(FutureDHT future) throws Exception {
                if(future.isSuccess()) {
                    UserData user = (UserData) future.getData().getObject();
                    String passwordHash = user.getPasswordHash();
                    if (PasswordHash.validatePassword(password, passwordHash)) {
                        ServiceServerHandler handler = ((ServiceServerHandler) ctx.handler());
                        handler.setAuthenticated(true);
                        handler.setUsername(username);
                        RPCResponse response = loginSuccess(username, requestId);
                        writeResponse(response);
                    } else {
                        RPCResponse response = loginError(requestId);
                        writeResponse(response);
                    }
                } else {
                    RPCResponse response = loginError(requestId);
                    writeResponse(response);
                }
            }
        });
    }

    public RPCResponse loginError(String requestId) {
        RPCResponse response = new RPCResponse();
        response.setError(Error.ERR_INVALID_USER_CODE, Error.ERR_INVALID_USER_MSG);
        response.setId(requestId);
        return response;
    }

    public RPCResponse loginSuccess(String userName, String requestId) {
        RPCResponse response = new RPCResponse();
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("username", userName);
        response.setResult(result);
        response.setId(requestId);
        return response;
    }
}
