package tecnico.kkn.service.search;

import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represent a query
 */
public class Query {
    private final static Logger log = LoggerFactory.getLogger(Query.class);
    private OPERATOR operator;
    private Query left;
    private Query right;
    private String keyword;

    public void setOperator(OPERATOR operator) {
        this.operator = operator;
    }

    public void setLeft(Query left) {
        this.left = left;
    }

    public void setRight(Query right) {
        this.right = right;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * Execute the query on the DHT and return a map of the result
     *
     * @return The map of result
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Map<Number160, Data> execute(Node node) throws IOException, ClassNotFoundException {
        Map<Number160, Data> leftResult, rightResult, result;
        if (left == null || right == null) {
            Peer p = node.getNextPeer();
            Number160 keywordHash = Number160.createHash(keyword);
            log.debug("Keyword hash {}", keywordHash);
            FutureDHT future = p.get(keywordHash).setDomainKey(Node.ITEM_INDEX_DOMAIN).setAll().start();
            future.awaitUninterruptibly();

            if(future.isSuccess()) {
                result = future.getDataMap();
            } else {
                result = new HashMap<Number160, Data>();
            }
        } else {
            leftResult = left.execute(node);
            rightResult = right.execute(node);
            result = new HashMap<Number160, Data>();
            switch (operator) {
                case AND:
                    for (Number160 hash: leftResult.keySet()) {
                        if (rightResult.keySet().contains(hash)) {
                            result.put(hash, leftResult.get(hash));
                        }
                    }
                    break;
                case OR:
                    for (Number160 hash: leftResult.keySet()) {
                        result.put(hash, leftResult.get(hash));
                    }
                    for (Number160 hash: rightResult.keySet()) {
                        result.put(hash, rightResult.get(hash));
                    }
                    break;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        if (left == null || right == null) {
            return keyword;
        } else {
            return "( " + left.toString() + " " + operator + " " + right.toString() + " )";
        }
    }
}
