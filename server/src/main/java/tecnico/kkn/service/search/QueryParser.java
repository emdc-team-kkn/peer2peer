package tecnico.kkn.service.search;

import tecnico.kkn.exceptions.InvalidQueryException;

import java.util.Stack;

/**
 * This class parse a query string and return a
 */
public class QueryParser {

    /**
     * Parse the string query into a query tree that can be execute against our DHT
     * @param query The query string
     * @return The query tree
     * @throws InvalidQueryException
     */
    public Query parse(String query) throws InvalidQueryException {
        Stack<Query> queryStack = new Stack<Query>();
        Stack<OPERATOR> operatorStack = new Stack<OPERATOR>();

        String[] tokens = query.trim().toLowerCase().split(" ");

        int counter = 0;

        /**
         * Precedence rules
         * ( ) : 1
         * AND : 2
         * OR : 3
         */
        while(counter < tokens.length) {
            String token = tokens[counter];
            if (token.equals("(")) {
                // Encounter left parentheses, push it to operator stack
                operatorStack.push(OPERATOR.LPAREN);
            } else if (token.equals(")")) {
                // Encounter right parentheses, pop until encounter left parentheses
                OPERATOR operator;
                while(!operatorStack.empty()) {
                    operator = operatorStack.pop();
                    if (operator.equals(OPERATOR.LPAREN)) {
                        break;
                    }
                    Query q1 = queryStack.pop();
                    Query q2 = queryStack.pop();
                    Query q3 = new Query();
                    q3.setLeft(q1);
                    q3.setRight(q2);

                    if (operator.equals(OPERATOR.AND)) {
                        q3.setOperator(OPERATOR.AND);
                    } else {
                        q3.setOperator(OPERATOR.OR);
                    }
                    queryStack.push(q3);
                }
            } else if (token.equals("and")) {
                // Encounter "AND" token
                // Push it on to the stack
                operatorStack.push(OPERATOR.AND);

            } else if (token.equals("or")) {
                // Encounter "OR" token
                if (!operatorStack.isEmpty()) {
                    // Stack is not empty
                    OPERATOR operator = operatorStack.peek();
                    if (operator.equals(OPERATOR.AND)) {
                        // If top of the stack is the AND operator
                        // pop the top operator of the stack & build query
                        // then push the result back onto the token stack
                        operator = operatorStack.pop();
                        Query q1 = queryStack.pop();
                        Query q2 = queryStack.pop();
                        Query q3 = new Query();
                        q3.setLeft(q1);
                        q3.setRight(q2);
                        q3.setOperator(OPERATOR.AND);
                        queryStack.push(q3);
                    }
                }
                operatorStack.push(OPERATOR.OR);
            } else {
                Query q = new Query();
                q.setKeyword(token);
                queryStack.push(q);

                // OR operator is optional, so we look ahead one token to see
                // if we need to insert the OR operator into the operator stack
                if (counter < tokens.length - 1) {
                    String nextToken = tokens[counter+1];
                    if (!nextToken.equals("(") &&
                            !nextToken.equals(")") &&
                            !nextToken.equals("and") &&
                            !nextToken.equals("or")) {
                        if (!operatorStack.isEmpty()) {
                            // Stack is not empty
                            OPERATOR operator = operatorStack.peek();
                            if (operator.equals(OPERATOR.AND)) {
                                // If top of the stack is the AND operator
                                // pop the top operator of the stack & build query
                                // then push the result back onto the token stack
                                operator = operatorStack.pop();
                                Query q1 = queryStack.pop();
                                Query q2 = queryStack.pop();
                                Query q3 = new Query();
                                q3.setLeft(q1);
                                q3.setRight(q2);
                                q3.setOperator(OPERATOR.AND);
                                queryStack.push(q3);
                            }
                        }
                        operatorStack.push(OPERATOR.OR);
                    }
                }
            }
            counter++;
        }

        while(!operatorStack.empty() && queryStack.size() > 1) {
            OPERATOR operator = operatorStack.pop();
            if (operator.equals(OPERATOR.LPAREN)) {
                throw new InvalidQueryException("Miss match parentheses");
            } else {
                Query q1 = queryStack.pop();
                Query q2 = queryStack.pop();
                Query q3 = new Query();
                q3.setLeft(q1);
                q3.setRight(q2);
                q3.setOperator(operator);
                queryStack.push(q3);
            }
        }

        return queryStack.pop();
    }
}
