package tecnico.kkn.p2p;

import java.io.Serializable;

/**
 * This class store the item data
 */
public class ItemData implements Serializable {

    static final long serialVersionUID = -3042542722173016234L;

    private String itemId;
    private String title;
    private String description;
    private String ownerName;
    private Boolean purchased = false;

    private double maxBid = 0;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Boolean getPurchased() {
        return purchased;
    }

    public void setPurchased(Boolean purchased) {
        this.purchased = purchased;
    }

    public void setMaxBid(double maxBid) {
        this.maxBid = maxBid;
    }

    public double getMaxBid() {
        return maxBid;
    }

    @Override
    public String toString() {
        return "ItemData{" +
                "itemId='" + itemId + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", purchased=" + purchased +
                ", maxBid=" + maxBid +
                '}';
    }
}
