package tecnico.kkn.p2p;

import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.p2p.RequestP2PConfiguration;
import net.tomp2p.peers.*;
import net.tomp2p.storage.StorageGeneric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.statistics.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * This is a Node in our P2P system
 * It maintains a list a virtual peers.
 */
public class Node {
    private static final Logger log = LoggerFactory.getLogger(Node.class);

    private ScheduledExecutorService executorService;
    private List<ScheduledFuture> scheduledFutures;
    private final NodeStatistics nodeStatistics;
    private static final int GOSSIP_FREQ = 6;

    private List<Peer> peers;
    private Set<Number160> peersSet;

    private int currentPeer = -1;
    private int peerCount;

    private boolean isGossiping = false;

    public static final Number160 USER_DOMAIN = Number160.createHash("USER_DOMAIN");
    public static final Number160 ITEM_DOMAIN = Number160.createHash("ITEM_DOMAIN");
    public static final Number160 ITEM_INDEX_DOMAIN = Number160.createHash("ITEM_INDEX_DOMAIN");
    public static final Number160 BID_DOMAIN = Number160.createHash("BID_DOMAIN");
    public static final Number160 ITEM_BID_DOMAIN = Number160.createHash("ITEM_BID_DOMAIN");
    public static final Number160 USER_BID_DOMAIN = Number160.createHash("USER_BID_DOMAIN");
    public static final Number160 USER_ITEM_DOMAIN = Number160.createHash("USER_ITEM_DOMAIN");

    public Node(List<Peer> peers, NodeStatistics nodeStatistics) {
        this.peers = peers;
        this.peerCount = peers.size();
        this.executorService = Executors.newScheduledThreadPool(5);
        this.scheduledFutures = new ArrayList<ScheduledFuture>();
        this.nodeStatistics = nodeStatistics;
        this.peersSet = new HashSet<Number160>();
        for(Peer p: peers) {
            p.setObjectDataReply(new GossipReply(this));
            this.peersSet.add(p.getPeerID());
        }

        // Reset the gossip every 10 minutes
        int resetTimer = 10;
        Calendar c = Calendar.getInstance();
        int currentMinute = c.get(Calendar.MINUTE);
        int deltaMinute = (((currentMinute / resetTimer) + 1) * resetTimer) - currentMinute;
        c.add(Calendar.MINUTE, deltaMinute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        log.debug("Will start gossiping at  {}", c.getTime());
        this.executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                stopAndResetGossip();
            }
        }, c.getTimeInMillis() - System.currentTimeMillis(), resetTimer * 60 * 1000, TimeUnit.MILLISECONDS);

        //Schedule node maintainance
        //Ironically, this make the P2P network less stable. May be there are some bugs.
        //FIXME: Debug this
        this.executorService.scheduleWithFixedDelay(new NodeMaintainer(this), 1, 15, TimeUnit.MINUTES);
    }

    /**
     * @return the node's statistics data
     */
    public NodeStatistics getNodeStatistics() {
        return nodeStatistics;
    }

    /**
     * We choose the virtual peer to query based on a Round-Robin fashion
     * @return The chosen peer
     */
    public Peer getNextPeer() {
        this.currentPeer = (this.currentPeer + 1) % this.peerCount;
        return this.peers.get(this.currentPeer);
    }

    /**
     * Start gossiping protocol
     */
    synchronized public void startGossip() {
        if (isGossiping) {
            return;
        }
        log.debug("Start gossiping");
        isGossiping = true;
        //Count the user & item that this node is responsible for
        Set<Number160> usersSet = new HashSet<Number160>();
        Set<Number160> itemsSet = new HashSet<Number160>();
        for (Peer p : peers) {
            StorageGeneric storageGeneric = p.getPeerBean().getStorage();
            for (Number480 key : storageGeneric.map().keySet()) {
                Number160 peersId = storageGeneric.findPeerIDForResponsibleContent(key.getLocationKey());
                if(peersSet.contains(peersId)) {
                    if (key.getDomainKey().equals(USER_DOMAIN)) {
                        usersSet.add(key.getLocationKey());
                    } else if (key.getDomainKey().equals(ITEM_DOMAIN)) {
                        try {
                            Object rawItem = storageGeneric.map().get(key).getObject();
                            log.debug("Item value: {}", rawItem);
                            ItemData itemData = (ItemData) rawItem;

                            log.debug("Item: {}", itemData.getPurchased());
                            if (itemData.getPurchased() == null || !itemData.getPurchased()) {
                                itemsSet.add(key.getLocationKey());
                            }
                        } catch (Exception e) {
                            log.warn("Error parsing item data", e);
                        }
                    }
                }
            }
        }
        nodeStatistics.getUserData().setSum(usersSet.size());
        nodeStatistics.getItemData().setSum(itemsSet.size());
        scheduledFutures.add(executorService.scheduleWithFixedDelay(
                new GossipExecutor(this),
                0,
                GOSSIP_FREQ,
                TimeUnit.SECONDS
        ));
    }

    /**
     * Stop gossiping and reset after a while
     */
    synchronized public void stopAndResetGossip() {
        log.debug("Stop && reset gossipping.");
        for(ScheduledFuture future: scheduledFutures) {
            future.cancel(false);
        }
        scheduledFutures.clear();
        isGossiping = false;

        // Wait for a bit for all message currently in transit to clear out.
        int snapshotDelay = 15;
        log.debug("Resetting in {} seconds", snapshotDelay);

        executorService.schedule(new Runnable() {
            @Override
            public void run() {
                synchronized (nodeStatistics) {
                    nodeStatistics.takeSnapshot();
                    log.info("Snapshot node count: {}", nodeStatistics.getApproximateNodeCount());
                    log.info("Snapshot user count: {}", nodeStatistics.getApproximateUserCount());
                    log.info("Snapshot item count: {}", nodeStatistics.getApproximateItemCount());
                    nodeStatistics.getNodeData().setWeight(0);
                    nodeStatistics.getNodeData().setSum(1);
                    nodeStatistics.getUserData().setSum(0);
                    nodeStatistics.getUserData().setWeight(0);
                    nodeStatistics.getItemData().setWeight(0);
                    nodeStatistics.getItemData().setSum(0);
                }
                Peer p = getNextPeer();
                RequestP2PConfiguration configuration = new RequestP2PConfiguration(1, 5, 0);
                FutureDHT future = p.send(Number160.ZERO)
                        .setObject(new ResetGossipRequest())
                        .setRequestP2PConfiguration(configuration)
                        .start();
                future.addListener(new BaseFutureAdapter<FutureDHT>() {
                    @Override
                    public void operationComplete(FutureDHT future) throws Exception {
                        if (future.isSuccess()) {
                            startGossip();
                        } else {
                            log.debug("Unable to send: {}", future.getFailedReason());
                        }
                    }
                });
            }
        }, snapshotDelay, TimeUnit.SECONDS);
    }

    public List<Peer> getPeers() {
        return peers;
    }

    public boolean isGossiping() {
        return isGossiping;
    }
}

