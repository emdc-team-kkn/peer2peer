package tecnico.kkn.p2p.statistics;

import net.tomp2p.peers.PeerAddress;
import net.tomp2p.rpc.ObjectDataReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;

/**
 * This class handle the reply for gossip message
 */
public class GossipReply implements ObjectDataReply {
    private final static Logger log = LoggerFactory.getLogger(GossipReply.class);

    private final Node node;

    public GossipReply(Node node) {
        this.node = node;
    }

    @Override
    public Object reply(PeerAddress sender, Object request) throws Exception {
        if (request instanceof NodeStatistics) {
            NodeStatistics receive = (NodeStatistics) request;
            synchronized (node) {
                // Don't accept the data if the node is not gossiping
                if (!node.isGossiping()) {
                    return false;
                }
                node.getNodeStatistics().add(receive);

                GossipData nodeData = node.getNodeStatistics().getNodeData();
                GossipData itemData = node.getNodeStatistics().getItemData();
                GossipData userData = node.getNodeStatistics().getUserData();
                log.debug("Node count: {}", nodeData.getSum() / nodeData.getWeight());
                log.debug("User count: {}", userData.getSum() / userData.getWeight());
                log.debug("Item count: {}", itemData.getSum() / itemData.getWeight());
            }
        } else if (request instanceof ResetGossipRequest) {
            synchronized (node) {
                if (node.isGossiping()) {
                    // The node has already started gossiping, return immediately
                    return true;
                }
                log.debug("Resetting request received");
                node.getNodeStatistics().getNodeData().setSum(1);
                node.getNodeStatistics().getNodeData().setWeight(1);
                node.getNodeStatistics().getUserData().setSum(0);
                node.getNodeStatistics().getUserData().setWeight(1);
                node.getNodeStatistics().getItemData().setSum(0);
                node.getNodeStatistics().getItemData().setWeight(1);
                node.startGossip();
            }
        } else {
            log.debug("Receive: {}", request);
        }
        return true;
    }
}
