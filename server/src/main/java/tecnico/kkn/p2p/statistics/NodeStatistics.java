package tecnico.kkn.p2p.statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Date;

/**
 * This store statistic data for a node
 */
public class NodeStatistics implements Serializable {
    private final static Logger log = LoggerFactory.getLogger(NodeStatistics.class);

    private static final long serialVersionUID = 6222457796722504678L;
    private final GossipData nodeData;
    private final GossipData userData;
    private final GossipData itemData;
    private String statisticFilePath;

    private double approximateNodeCount = -1;
    private double approximateUserCount = -1;
    private double approximateItemCount = -1;

    public NodeStatistics(GossipData nodeData, GossipData userData, GossipData itemData) {
        this.nodeData = nodeData;
        this.userData = userData;
        this.itemData = itemData;
    }

    public void setStatisticFilePath(String statisticFilePath) {
        this.statisticFilePath = statisticFilePath;
    }

    public GossipData getNodeData() {
        return nodeData;
    }

    public GossipData getUserData() {
        return userData;
    }

    public GossipData getItemData() {
        return itemData;
    }

    synchronized public void add(NodeStatistics other) {
        this.nodeData.add(other.nodeData);
        this.userData.add(other.userData);
        this.itemData.add(other.itemData);
    }

    synchronized public void minus(NodeStatistics other) {
        this.nodeData.minus(other.nodeData);
        this.userData.minus(other.userData);
        this.itemData.minus(other.itemData);
    }

    /**
     * @return the approximate number of node in P2PBay
     */
    public double getApproximateNodeCount() {
        return approximateNodeCount;
    }

    /**
     * @return the approximate number of user registered in P2PBay
     */
    public double getApproximateUserCount() {
        return approximateUserCount;
    }

    public double getApproximateItemCount() {
        return approximateItemCount;
    }

    synchronized public void takeSnapshot() {
        approximateNodeCount = nodeData.getSum() / nodeData.getWeight();
        approximateItemCount = itemData.getSum() / nodeData.getWeight();
        approximateUserCount = userData.getSum() / userData.getWeight();

        try {
            log.debug("Saving node statistic to file {}", statisticFilePath);
            PrintWriter writer = new PrintWriter(new FileOutputStream(statisticFilePath, true));
            writer.printf("Snapshot time: %s\n", new Date());
            writer.printf("Approximate node count: %f\n", approximateNodeCount);
            writer.printf("Approximate item count: %f\n", approximateItemCount);
            writer.printf("Approximate user count: %f\n", approximateUserCount);
            writer.println();
            writer.close();
        } catch (Exception e) {
            log.error("Unable to write statistics to file.", e);
        }
    }
}
