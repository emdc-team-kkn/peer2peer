package tecnico.kkn.p2p.statistics;

import java.io.Serializable;

/**
 * Gossip data to send to other peers
 */
public class GossipData implements Serializable{

    private static final long serialVersionUID = 490569374980999684L;
    private double weight;
    private double sum;

    public GossipData(double weight, double sum) {
        this.weight = weight;
        this.sum = sum;
    }

    public double getWeight() {
        return weight;
    }

    synchronized public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getSum() {
        return sum;
    }

    synchronized public void setSum(double sum) {
        this.sum = sum;
    }

    synchronized public void addSum(double sum) {
        this.sum += sum;
    }

    synchronized public void add(GossipData other) {
        this.sum += other.sum;
        this.weight += other.weight;
    }

    synchronized public void minus(GossipData other) {
        this.sum -= other.sum;
        this.weight -= other.weight;
    }
}
