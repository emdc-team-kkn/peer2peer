package tecnico.kkn.p2p.statistics;

import net.tomp2p.futures.BaseFutureAdapter;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.p2p.RequestP2PConfiguration;
import net.tomp2p.peers.Number160;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;

import java.util.Random;

/**
 * This class handle sending gossip message from our node
 */
public class GossipExecutor implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(GossipExecutor.class);
    private final Node node;

    public GossipExecutor(Node n) {
        this.node = n;
    }

    @Override
    public void run() {
        synchronized (node) {
            final double nodeWeightSent = node.getNodeStatistics().getNodeData().getWeight() / 2;
            final double nodeSumSent = node.getNodeStatistics().getNodeData().getSum() / 2;
            final double userWeightSent = node.getNodeStatistics().getUserData().getWeight() / 2;
            final double userSumSent = node.getNodeStatistics().getUserData().getSum() / 2;
            final double itemWeightSent = node.getNodeStatistics().getItemData().getWeight() / 2;
            final double itemSumSent = node.getNodeStatistics().getItemData().getSum() / 2;

            GossipData nodeCount = new GossipData(nodeWeightSent, nodeSumSent);
            GossipData userCount = new GossipData(userWeightSent, userSumSent);
            GossipData itemCount = new GossipData(itemWeightSent, itemSumSent);

            final NodeStatistics statisticsSent = new NodeStatistics(nodeCount, userCount, itemCount);
            // We pick a random id and send a message to the node closest to that id
            RequestP2PConfiguration configuration = new RequestP2PConfiguration(1, 5, 0);
            Peer peer = node.getNextPeer();
            FutureDHT future = peer.send(new Number160(new Random())).setRequestP2PConfiguration(configuration)
                    .setObject(statisticsSent)
                    .start();
            future.addListener(new BaseFutureAdapter<FutureDHT>() {
                @Override
                public void operationComplete(FutureDHT future) throws Exception {
                    if (future.isSuccess()) {
                        Boolean accepted = (Boolean) future.getObject();
                        if (accepted) {
                            synchronized (node) {
                                node.getNodeStatistics().minus(statisticsSent);
                            }
                        }
                    }
                }
            }, true);
        }
    }
}
