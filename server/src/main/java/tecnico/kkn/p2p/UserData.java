package tecnico.kkn.p2p;

import java.io.Serializable;

/**
 * This class store user data
 */
public class UserData implements Serializable {
    static final long serialVersionUID = 288234559027488149L;
    private String username;
    private String passwordHash;

    public UserData() {
    }

    public UserData(String username, String passwordHash) {
        this.username = username;
        this.passwordHash = passwordHash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

}
