package tecnico.kkn.p2p;

import net.tomp2p.futures.FutureBootstrap;
import net.tomp2p.futures.FutureDiscover;
import net.tomp2p.p2p.Peer;
import net.tomp2p.p2p.PeerMaker;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.StorageGeneric;
import tecnico.kkn.p2p.statistics.GossipData;
import tecnico.kkn.p2p.statistics.NodeStatistics;
import tecnico.kkn.storage.StorageMapDB;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to create a p2p in our P2P system
 */
public class NodeFactory {

    private final String storagePath;
    private final String statisticFilePath;

    public NodeFactory(String storagePath, String statisticFilePath) {
        this.storagePath = storagePath;
        this.statisticFilePath = statisticFilePath;
    }

    /**
     * This method create a new bootstrap node. This node are the first peer in the entire network.
     * It's not bootstrapped to any known peer
     *
     * @param nodeId The id of the node
     * @param port The port to bind to
     * @param virtualNodes The number of virtual node to create
     * @return A node in our P2P system
     * @throws IOException
     */
    public Node newBootstrapNode(String nodeId, int port, int virtualNodes) throws IOException {
        List<Peer> peers = new ArrayList<Peer>(virtualNodes);
        Number160 nodekey = Number160.createHash(nodeId);
        //Create storage folder if not exist
        File store = new File(storagePath, nodekey.toString());
        if (!store.exists()) {
            store.mkdirs();
        }
        StorageGeneric storage = new StorageMapDB(store.getAbsolutePath());
        PeerMaker pm = new PeerMaker(nodekey).setPorts(port).setStorage(storage)
                .setEnableIndirectReplication(true);
        Peer peer = pm.makeAndListen();
        peers.add(peer);
        List<Peer> virtualPeers = createSecondaryPeer(nodeId, virtualNodes - 1, peer);
        peers.addAll(virtualPeers);

        GossipData nodeCount = new GossipData(1, 1);
        GossipData userCount = new GossipData(1, 0);
        GossipData itemCount = new GossipData(1, 0);
        NodeStatistics nodeStatistics = new NodeStatistics(nodeCount, userCount, itemCount);
        nodeStatistics.setStatisticFilePath(statisticFilePath);
        return new Node(peers, nodeStatistics);
    }

    /**
     * This method create a new node and bootstrap it to an existing node base on address
     *
     * @param nodeId The id of the node
     * @param port The port to bind to
     * @param virtualNodes The number of virtual node to create
     * @param bootstrapAddress The address of node we bootstrap to
     * @param bootstrapPort The port of the node we bootsrap to
     * @return A node in our P2P system
     * @throws IOException
     */
    public Node newNormalNode(String nodeId, int port, int virtualNodes,
                              InetAddress bootstrapAddress, int bootstrapPort) throws IOException {
        List<Peer> peers = new ArrayList<Peer>(virtualNodes);
        Number160 nodekey = Number160.createHash(nodeId);
        //Create storage folder if not exist
        File store = new File(storagePath, nodekey.toString());
        if (!store.exists()) {
            store.mkdirs();
        }
        StorageGeneric storage = new StorageMapDB(store.getAbsolutePath());
        PeerMaker pm = new PeerMaker(nodekey).setPorts(port).setStorage(storage)
                .setEnableIndirectReplication(true);
        Peer peer = pm.makeAndListen();
        FutureBootstrap fb = peer.bootstrap().setInetAddress(bootstrapAddress).setPorts(bootstrapPort).start();
        fb.awaitUninterruptibly();
        if (fb.getBootstrapTo() != null) {
            peer.discover().setPeerAddress(fb.getBootstrapTo().iterator().next()).start().awaitUninterruptibly();
        }
        peers.add(peer);

        List<Peer> virtualPeers = createSecondaryPeer(nodeId, virtualNodes - 1, peer);
        peers.addAll(virtualPeers);

        GossipData nodeCount = new GossipData(0, 1);
        GossipData userCount = new GossipData(0, 0);
        GossipData itemCount = new GossipData(0, 0);
        NodeStatistics nodeStatistics = new NodeStatistics(nodeCount, userCount, itemCount);
        nodeStatistics.setStatisticFilePath(statisticFilePath);
        return new Node(peers, nodeStatistics);
    }

    /**
     * Our node maintains a list of virtual peers, one master and multiple secondary
     * This method create the secondary peers in our Node
     * @param baseId The baseId of the virtual peers
     * @param peerCount The number of virtual peer to create
     * @param masterPeer The address of the master peer
     * @return A list of secondary peer
     * @throws IOException
     */
    private List<Peer> createSecondaryPeer(String baseId, int peerCount, Peer masterPeer) throws IOException {
        List<Peer> peers = new ArrayList<Peer>(peerCount);
        for(int i = 1; i <= peerCount; i++) {
            Number160 nodekey = Number160.createHash(baseId + "-" + i);
            //Create storage folder if not exist
            File store = new File(storagePath, nodekey.toString());
            if (!store.exists()) {
                store.mkdirs();
            }
            StorageGeneric storage = new StorageMapDB(store.getAbsolutePath());
            PeerMaker pm = new PeerMaker(nodekey)
                    .setMasterPeer(masterPeer)
                    .setEnableIndirectReplication(true)
                    .setStorage(storage);
            Peer peer = pm.makeAndListen();
            FutureBootstrap fb = peer.bootstrap().setPeerAddress(masterPeer.getPeerAddress()).start();
            fb.awaitUninterruptibly();
            if (fb.getBootstrapTo() != null) {
                FutureDiscover fd = peer.discover().setPeerAddress(fb.getBootstrapTo().iterator().next()).start();
                fd.awaitUninterruptibly();
            }
            peers.add(peer);
        }
        return peers;
    }
}
