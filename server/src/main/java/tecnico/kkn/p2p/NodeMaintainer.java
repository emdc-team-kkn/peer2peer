package tecnico.kkn.p2p;

import net.tomp2p.futures.FutureDHT;
import net.tomp2p.p2p.Peer;
import net.tomp2p.peers.Number160;
import net.tomp2p.peers.Number480;
import net.tomp2p.storage.Data;
import net.tomp2p.storage.StorageGeneric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * An instance of this class is periodically run to maintain the
 * item index
 */
class NodeMaintainer implements Runnable {

    private final static Logger log = LoggerFactory.getLogger(NodeMaintainer.class);

    private final Node node;

    public NodeMaintainer(Node node) {
        this.node = node;
    }

    @Override
    public void run() {
        log.debug("Performing item index maintenance");
        List<Peer> peers = node.getPeers();
        for (Peer p : peers) {
            try {
                indexItem(p);
            } catch (RuntimeException e) {
                log.warn("Runtime exception", e);
            }
        }
        log.debug("Item index maintenance completed");
    }

    private void indexItem(final Peer p) {
        log.debug("Indexing peer: {}", p.getPeerID());
        StorageGeneric storageGeneric = p.getPeerBean().getStorage();
        Set<Number480> keyset = storageGeneric.map().keySet();
        Number160 pId = p.getPeerID();
        for (final Number480 key : keyset) {
            Number160 peerId = storageGeneric.findPeerIDForResponsibleContent(key.getLocationKey());
            if (pId.equals(peerId)) {
                try {
                    if (key.getDomainKey().equals(Node.ITEM_DOMAIN)) {
                        // Re indexing item
                        Object rawData = storageGeneric.map().get(key).getObject();
                        log.debug("Raw data: {}", rawData);
                        ItemData data = (ItemData) rawData;
                        log.debug("Item data: {}", data);
                        String title = data.getTitle();
                        if (title != null) {
                            String[] tokens = title.toLowerCase().split("\\s+");

                            for (String token : tokens) {
                                Number160 tokenKey = Number160.createHash(token);
                                p.put(tokenKey).setDomainKey(Node.ITEM_INDEX_DOMAIN)
                                        .setData(Number160.createHash(data.getItemId()), new Data(data.getItemId()))
                                        .start()
                                        .awaitUninterruptibly();
                            }
                        }
                    } else if (key.getDomainKey().equals(Node.ITEM_INDEX_DOMAIN)) {
                        // Remove index that point to invalid item
                        // We use synchronous request here to avoid overwhelming the DHT
                        final Number160 itemIdHash = key.getContentKey();
                        FutureDHT f = p.get(itemIdHash)
                                .setDomainKey(Node.ITEM_DOMAIN)
                                .start()
                                .awaitUninterruptibly();

                        if (!f.isSuccess()) {
                            log.warn("Invalid item, removing {}", itemIdHash);
                            //Item no longer exists, remove it from index
                            p.remove(key.getLocationKey())
                                    .setDomainKey(key.getDomainKey())
                                    .setContentKey(key.getContentKey())
                                    .start();
                        }
                    }
                } catch (ClassCastException e) {
                    p.remove(key.getLocationKey())
                            .setDomainKey(key.getDomainKey())
                            .setRepetitions(6).start();
                    log.debug("Invalid data type", e);
                } catch (ClassNotFoundException | IOException e) {
                    log.debug("Error when maintaining item index", e);
                }
            }
        }
        log.debug("Finish processing: {}", p.getPeerID());
    }
}
