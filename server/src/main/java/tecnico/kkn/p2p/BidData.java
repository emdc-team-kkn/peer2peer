package tecnico.kkn.p2p;

import java.io.Serializable;

/**
 * This class store bid data
 */
public class BidData implements Serializable {

    private static final long serialVersionUID = 1154880134110990383L;
    private double bidValue;
    private String bidId;
    private String itemId;
    private String userId;

    private Boolean purchased = false;

    public double getBidValue() {
        return bidValue;
    }

    public void setBidValue(double bidValue) {
        this.bidValue = bidValue;
    }

    public String getBidId() {
        return bidId;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getPurchased() {
        return purchased;
    }

    public void setPurchased(Boolean purchased) {
        this.purchased = purchased;
    }
}
