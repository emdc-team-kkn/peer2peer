package tecnico.kkn.exceptions;

/**
 * This exception is thrown when we cannot parse an query
 */
public class InvalidQueryException extends Exception {

    private static final long serialVersionUID = 1999690401445480901L;

    public InvalidQueryException(String message) {
        super(message);
    }
}
