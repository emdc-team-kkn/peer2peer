package tecnico.kkn.runner;

import org.kohsuke.args4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.p2p.NodeFactory;
import tecnico.kkn.network.ServiceServer;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * The Runner class of the program contain the main method
 */
public class Runner {
    private static final Logger log = LoggerFactory.getLogger(Runner.class);

    @Option(name = "-id",
            usage = "The id of the node. Default: network address of the current machine",
            metaVar = "<node id>")
    private String nodeId;

    @Option(name = "-burl",
            usage = "The address of the bootstrap node, leave blank to self-bootstrap",
            metaVar = "<hostname>",
            depends = {"-bport"})
    private String bootstrapUrl = null;

    @Option(name = "-bport",
            usage = "The port of the bootstrap node",
            metaVar = "<port>")
    private int bootstrapPort;

    @Option(name = "-storage",
            usage = "The directory to store data. Default: server/dht-storage",
            metaVar = "<storage path>")
    private String storagePath = "server/dht-storage";

    @Option(name = "-dhtPort",
            required = true,
            usage = "The port that our DHT will operate",
            metaVar = "<port>")
    private int dhtPort;

    @Option(name = "-servicePort",
            required = true,
            usage = "The port that our cmdline client connect to",
            metaVar = "<port>")
    private int servicePort;

    @Option(name = "-vpeer",
            usage = "The number of virtual peers per node. Default: 1",
            metaVar = "<peer count>")
    private int virtualPeers = 1;

    @Option(name = "-sfile",
            usage = "The location of the statistic file. Default: server/log/{nodeid}-statistics.txt",
            metaVar = "<statistic file path>")
    private String statisticFilePath;

    // receives other command line parameters than options
    @Argument
    private List<String> arguments = new ArrayList<String>();

    public void run(String[] args) {
        ParserProperties parserProperties = ParserProperties.defaults();
        parserProperties.withUsageWidth(80);

        CmdLineParser parser = new CmdLineParser(this, parserProperties);

        try {
            parser.parseArgument(args);

            if (nodeId == null) {
                nodeId = InetAddress.getLocalHost().getHostAddress();
            }

            if (statisticFilePath == null) {
                statisticFilePath = String.format("server/logs/%s-statistics.txt", nodeId);
            }

            NodeFactory nodeFactory = new NodeFactory(storagePath, statisticFilePath);
            Node node;
            if (bootstrapUrl == null || bootstrapPort == -1) {
                node = nodeFactory.newBootstrapNode(nodeId, dhtPort, virtualPeers);
            } else {
                InetAddress bootstrapAddress = InetAddress.getByName(bootstrapUrl);
                node = nodeFactory.newNormalNode(
                        nodeId, dhtPort, virtualPeers,
                        bootstrapAddress, bootstrapPort
                );
            }

            ServiceServer serviceServer = new ServiceServer(servicePort, node);
            serviceServer.run();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java tecnico.kkn.runner.Runner [options...] arguments...");
            parser.printUsage(System.err);
            parser.printExample(OptionHandlerFilter.ALL);
            System.err.println();
            System.exit(-3);
        } catch (IOException e) {
            log.error("Unable to startup a new node. Shutting down", e);
            System.exit(-1);
        } catch (Exception e) {
            log.error("Unable to startup service server. Shutting down", e);
            System.exit(-2);
        }
    }

    public static void main(String[] args) {
        new Runner().run(args);
    }
}
