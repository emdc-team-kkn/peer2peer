package tecnico.kkn.network;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tecnico.kkn.exceptions.InvalidQueryException;
import tecnico.kkn.p2p.Node;
import tecnico.kkn.service.*;
import tecnico.kkn.service.message.RPCResponse;
import tecnico.kkn.service.message.RPCRequest;
import tecnico.kkn.service.message.Error;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.ExecutorService;

/**
 * This class handle a connection to the client
 */
public class ServiceServerHandler extends ChannelInboundHandlerAdapter {

    private final static Logger log = LoggerFactory.getLogger(ServiceServerHandler.class);

    private boolean authenticated = false;
    private String username;
    private final Gson gson;
    private Node node;
    private final ExecutorService executorService;

    public ServiceServerHandler(Node node, ExecutorService executorService) {
        this.node = node;
        this.executorService = executorService;
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws IOException, InvalidQueryException, InvalidKeySpecException, NoSuchAlgorithmException {
        log.info("Authenticated: {}", authenticated);
        ByteBuf in = (ByteBuf) msg;
        String message = in.toString(CharsetUtil.UTF_8).toLowerCase();

        RPCRequest request = gson.fromJson(message, RPCRequest.class);
        String method = request.getMethod();
        if (method.equals("register")) {
            if (authenticated) {
                this.alreadyLoggedIn(ctx, request.getId());
            } else {
                RegisterService service = new RegisterService(node, ctx);
                service.register(
                        request.getParams().get("username"),
                        request.getParams().get("password"),
                        request.getId()
                );
            }
        } else if (method.equals("login")) {
            if (authenticated) {
                this.alreadyLoggedIn(ctx, request.getId());
            } else {
                LoginService service = new LoginService(node, ctx);
                service.login(
                        request.getParams().get("username"),
                        request.getParams().get("password"),
                        request.getId()
                );
            }
        } else if (method.equals("logout")) {
            LogoutService service = new LogoutService(node, ctx);
            service.logout(request.getId());
        } else if (method.equals("offer")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                OfferItemService service = new OfferItemService(node, ctx, executorService);
                service.offerItem(
                        request.getParams().get("title"),
                        request.getParams().get("description"),
                        request.getId()
                );
            }
        } else if (method.equals("offered")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                ViewOfferedService service = new ViewOfferedService(node, ctx, executorService);
                service.viewOffered(
                        request.getId()
                );
            }
        } else if (method.equals("search")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                SearchService service = new SearchService(node, ctx, executorService);
                service.search(
                        request.getParams().get("query"),
                        request.getParams().get("page"),
                        request.getId()
                );
            }
        } else if (method.equals("bid")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                BidService service = new BidService(node, ctx, executorService);
                service.bid(
                        request.getParams().get("item_id"),
                        Double.parseDouble(request.getParams().get("bid_value")),
                        username,
                        request.getId()
                );
            }
        } else if (method.equals("accept")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                AcceptBidService service = new AcceptBidService(node, ctx, executorService);
                service.accept(
                        request.getParams().get("item_id"),
                        request.getId()
                );
            }
        } else if (method.equals("history")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                BidHistoryService service = new BidHistoryService(node, ctx, executorService);
                service.history(
                        request.getId()
                );
            }
        } else if (method.equals("details")) {
            if (!authenticated) {
                this.notAuthenticated(ctx, request.getId());
            } else {
                DetailsService service = new DetailsService(node, ctx, executorService);
                service.details(
                        request.getParams().get("item_id"),
                        request.getId()
                );
            }
        }
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private void alreadyLoggedIn(ChannelHandlerContext ctx, String getId) {
        RPCResponse response = new RPCResponse();
        response.setId(getId);
        response.setError(Error.ERR_LOGGED_IN_CODE, Error.ERR_LOGGED_IN_MSG);
        writeResponse(ctx, response);
    }

    private void notAuthenticated(ChannelHandlerContext ctx, String requestId) {
        RPCResponse response = new RPCResponse();
        response.setId(requestId);
        response.setError(Error.ERR_NOT_AUTH_CODE, Error.ERR_NOT_AUTH_MSG);
        writeResponse(ctx, response);
    }

    private void writeResponse(ChannelHandlerContext ctx, RPCResponse response) {
        ByteBuf out = ctx.alloc().buffer();
        out.writeBytes(gson.toJson(response).getBytes(CharsetUtil.UTF_8));
        ctx.writeAndFlush(out);
    }
}
