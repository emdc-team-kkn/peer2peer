# Implementing a service

## On the client 
* Create a service class client in package `tecnico.kkn.service`.
This service will create a `RPCRequest` and send it to the server.
* Add a method in `ServiceClient` to call the service
* Modify method `run` in `CommandLine` to call 
the new method on `ServiceClient`
* Modify method `channelRead` in class `ServiceClientHandler` to process
the `RPCResponse` returned from the server.
* Example: Check existing service LoginService, RegisterService, QuitService.

## On the server
* Create a service class on the server in package `tecnico.kkn.service` to
handle the service. This method will interact with the DHT and will write a 
`RPCResponse` back to the client.
* Modify method `channelRead` in class `ServiceServerHandler` to call the service
class on the server.
* Example: Check existing service `LoginService`, `RegisterService`, `QuitService`.
